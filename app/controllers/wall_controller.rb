class WallController < ApplicationController

  def index

  end
  def print
    @files = Dir.glob("#{Rails.root}/public/uploads/*").select { |f| File.file?(f) }.sort_by{ |f| File.mtime(f) }.map{ |f| f.to_s.gsub(Rails.root.to_s + "/public", "")}

  end

  def pictures
    @files = Dir.glob("#{Rails.root}/public/uploads/*").select { |f| File.file?(f) }.sort_by{ |f| File.mtime(f) }.map{ |f| f.to_s.gsub(Rails.root.to_s + "/public", "")}

    render json: @files
  end


  def create
    uploaded_io = params[:image]
    File.open(Rails.root.join('public', 'uploads', rand(1...9999).to_s + '_' +   uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
    flash.now[:success] = "Upload Successful"
    render json: { success: true }
  end

  def upload_post
    uploaded_io = params[:image]
    File.open(Rails.root.join('public', 'uploads', rand(1...9999).to_s + '_' +   uploaded_io.original_filename), 'wb') do |file|
      file.write(uploaded_io.read)
    end
    respond_to do |format|
      format.html do
        flash.now[:success] = "Upload Successful"
        render("upload")
      end
      format.json do
        render json: {success: true}
      end
    end


  end

  def printOnly

  end
  def upload

  end
end
